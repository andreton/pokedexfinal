export interface PokemonId{
    id: number,
    name: string,
    pictureUrl: string
}