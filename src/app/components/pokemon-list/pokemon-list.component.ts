import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon.service';
import {MatGridListModule} from '@angular/material/grid-list';


@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  pokemonData: any[] = [];

  constructor(private pokemonService:PokemonService, private router:Router) { }

  ngOnInit(): void {
    this.getPokemons();
  }
  getPokemons() {
    this.pokemonService.getPokemons().subscribe(
      pokemons => {
        this.pokemonData = [...pokemons]
      },
      err => {
        console.log(err);
      }
    );
  }
  getPokemonId(pokemon){
    this.router.navigateByUrl(`pokemonDetails/${pokemon.id}`);
  }

}
