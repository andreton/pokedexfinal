import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FormsModule } from '@angular/forms';
import { TrainerService } from 'src/app/services/trainer.service';



@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  isLoading:boolean=false;
  trainer={
    name:""
  }
  constructor(private http: HttpClient, private pokemonService: PokemonService,private auth:AuthService, private router:Router,private trainerService:TrainerService) 
  { }
  ngOnInit(): void {
  }

  async onRegisterClicked(){
    try{
      this.isLoading=true;
      const result:any=await this.auth.register(this.trainer.name);
    }
    catch(e){
      console.error(e);
    }
    finally{
      this.isLoading=false;
      this.router.navigateByUrl('/trainer')
    }
  }
}
