import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PokemonId } from 'src/app/Interfaces/pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.scss']
})
export class TrainerPageComponent implements OnInit {
  trainer={
    name:''
  }
  arrayOfCatchedPokemonsId:any=[];
  catchedPokemonsWithNameAndImage:any=[];
  arrayOfAllPokemonsWithImage:any=[];

  constructor(private trainerService:TrainerService, private router:Router, private pokemonService:PokemonService) { }

  ngOnInit(): void {
    this.trainer.name=this.trainerService.getTrainerName();    
    this.checkThePokemonsOfTrainer();
  }
  startOver(){
    localStorage.removeItem("PokemonId");
    localStorage.removeItem("trainerName");

    this.router.navigateByUrl('/landingpage');
  }

  goToPokemonList(){  
    this.router.navigateByUrl('/pokemons');
  }
  checkThePokemonsOfTrainer(){
    if(localStorage.getItem("PokemonId")=='' || localStorage.getItem("PokemonId")==null){
      return;
    }
    else{
      this.arrayOfCatchedPokemonsId=JSON.parse(localStorage.getItem("PokemonId"));
    }

   }
   getPokemons(id) {
      this.pokemonService.fetchOnlyIdAndImage(id).subscribe(
      pokemons => {
        this.catchedPokemonsWithNameAndImage=pokemons;
        },
      err => {
        console.log(err);
      }
    );

    }
  
  

}
