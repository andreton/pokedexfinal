import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Pokemon } from 'src/app/Interfaces/pokemondetailed';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';


@Component({
  selector: 'app-pokemon-detailed',
  templateUrl: './pokemon-detailed.component.html',
  styleUrls: ['./pokemon-detailed.component.scss']
})
export class PokemonDetailedComponent implements OnInit {

  pokemonDetails: Observable<Pokemon>;
  constructor(private pokemonService:PokemonService, private activatedRouter:ActivatedRoute, private router:Router,private trainerService:TrainerService ) {
    //Getting the passed id from the pokemon list component
    this.activatedRouter.params.subscribe(
      params=>{
        this.pokemonDetails=pokemonService.fetchPokemonDetails(params['id']);
      }
    )
  }
  
  ngOnInit(): void {
  }
  //Method for catching pokemon and then redirecting
  catchPokemon(pokemon){ 
    this.trainerService.catchPokemon(pokemon);
    this.router.navigateByUrl('/trainer');

       }
  goBack(){
        this.router.navigateByUrl('/trainer');
       }

}
