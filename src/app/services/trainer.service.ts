import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PokemonId } from '../Interfaces/pokemon';
import { Pokemon } from '../Interfaces/pokemondetailed';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  arrayOfCatchedPokemons:any=[];

  constructor(private router:Router, private http: HttpClient) { }
  
 checkThePokemonsOfTrainer(){
  var storedNames = JSON.parse(localStorage.getItem("PokemonId"));
  this.arrayOfCatchedPokemons=storedNames;
 }
 getTrainerName(){
  return localStorage.getItem("trainerName");
}
catchPokemon(pokemonInput){ 
  const pokemon:PokemonId={
    id:pokemonInput.id,
    name:pokemonInput.name,
    pictureUrl:pokemonInput.image
  }
  
 // this.arrayOfCatchedPokemons.push(pokemon); 
  if(localStorage.getItem("PokemonId")=='' || localStorage.getItem("PokemonId")==null){
    this.arrayOfCatchedPokemons=[];
    this.arrayOfCatchedPokemons.push(pokemon);
    localStorage.setItem("PokemonId",JSON.stringify(this.arrayOfCatchedPokemons));
  }
  else{
    this.arrayOfCatchedPokemons=JSON.parse(localStorage.getItem("PokemonId"));
    this.arrayOfCatchedPokemons.push(pokemon);
    localStorage.setItem("PokemonId",JSON.stringify(this.arrayOfCatchedPokemons));
  }
}

}
