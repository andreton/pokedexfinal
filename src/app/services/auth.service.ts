import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //Function for adding the trainer name to the localstorage
  register(trainerName){
    localStorage.setItem('trainerName',JSON.stringify(trainerName));
  }

  constructor() { }
}
