import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private router:Router) { }

  checkIfTrainerExists(){
    //Method for getting key values of localstorage
     var values = [],
         keys = Object.keys(localStorage),
         i = keys.length;
     while ( i-- ) {
         values.push( localStorage.getItem(keys[i]) );
     }
     if (localStorage.getItem("trainerName") == null) {
        localStorage.clear();
         return false;
     }
     else{
      return true;
     }
 }
}
