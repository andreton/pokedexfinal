import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import { Pokemon } from '../Interfaces/pokemondetailed';
import { Observable } from 'rxjs';
import { PokemonId } from '../Interfaces/pokemon';
@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) 
  { }

  /*Function for getting image */ 
  getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, avatar: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }

  getPokemons(){
    return this.http.get<any>(`${environment.apiURL}?limit=150`).pipe(
      map(response=>{
        return response.results.map(pokemon=>{
          return {
                
                  ...this.getIdAndImage(pokemon.url),
                  ...pokemon
          }
        })
      })
    )
  }

  //Function for fetching pokemon details based on index 
  fetchPokemonDetails(index): Observable<Pokemon> {
    // Perform our get request using our endpoint constant
    return this.http.get(`${environment.apiURL}${index}`)
      .pipe(
        map((response: Response) => {
          // Map the response into our Pokemon object type
          console.log(response);
          return this.mapPokemon(response);
        })
      );
  }
  /*Mapping the pokemon data*/ 
  mapPokemon(details):Pokemon{
    const types:string[]=[];
    for (const i in details.types){
      if(details.types[i].type){
        types.push(details.types[i].type.name);
      }
    }
    const moves: string []=[];
    for (const j in details.moves){
      if(details.moves[j].move){
        moves.push(details.moves[j].move.name);
      }
    }
    const abilities: string[]=[];
    for (const k in details.abilities){
      if(details.abilities[k].ability){
        abilities.push(details.abilities[k].ability.name);
      }
    }
    const baseStats:string[]=[];
    for(const l in details.stats){
      if(details.stats[l].base_stat){
          baseStats.push(details.stats[l].stat.name,details.stats[l].base_stat);
      }
    }
    const pokemon:Pokemon={
      name:details.name,
      id:details.id,
      weight: details.weight,
      height: details.height,
      image: details.sprites.front_default,
      types:types,
      moves:moves,
      abilities:abilities,
      baseStats:baseStats};
    return pokemon;
  }

  /*This one is used for the trainer to display the pokemons that have been catched  */
  fetchOnlyIdAndImage(index): Observable<PokemonId> {
    // Perform our get request using our endpoint constant
    return this.http.get(`${environment.apiURL}${index}`)
      .pipe(
        map((response: Response) => {
          // Map the response into our Pokemon object type
          return this.mapPokemonIdName(response);
        })
      );
  }

  mapPokemonIdName(details){
    const pokemonId:PokemonId={
      id:details.id,
      name: details.name,
      pictureUrl: details.sprites.front_default
    };
    return pokemonId;
 
    
  }

    

}
  
