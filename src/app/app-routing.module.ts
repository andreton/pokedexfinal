import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { PokemonDetailedComponent } from './components/pokemon-detailed/pokemon-detailed.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { TrainerPageComponent } from './components/trainer-page/trainer-page.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: "pokemons",
    component: PokemonListComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landingpage'
  },
  {
    path:'trainer',
    component:TrainerPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'landingpage',
    component:LandingPageComponent
    },
  {
    path: 'pokemonDetails/:id',
    component: PokemonDetailedComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
